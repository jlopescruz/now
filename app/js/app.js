var formApp = angular.module('formApp', []);

formApp.controller('formAppCtrl', ['$scope', 'formService', function($scope, formService) {
	
	$scope.selectedMovie;
	$scope.movies = [ 'blue', 'grey', 'green' ];

	//$scope.inputErrorMsg = formService.errorMsgs.inputError;
	$scope.isFormVisible = true;

	$scope.submitForm = function (isValid) {
		
		if (isValid) {

			formService.saveForm($scope.user).then(
				function(){
					console.log('Success');
				},
				function(){
					console.error('Error');
				}
			);
		} 
	};

	function presentInvalidFields() {

		for (var property in $scope.userForm.$error) {

			var errors = $scope.userForm.$error[property];

			errors.forEach(function(error) {
				console.log(error.$name + ' field failed: ' + property);
			});
		};
	};

	$scope.updateSelectedMovie = function(movie) {
		$scope.selectedMovie = movie;
	};

	$scope.isSelected = function(movie) {
		return ($scope.selectedMovie === movie);
	};

}]);


formApp.factory('formService', ['$http', function($http) {

	var ERROR_MESSAGES = {};
	ERROR_MESSAGES.inputError = 'Required field';

	function saveForm( user ) {

		return $http.post(
            'endpoint/form/data',
            user
        );
	};
	
	// public api
	return {
		saveForm: saveForm,
		errorMsgs: ERROR_MESSAGES
	};
}]);