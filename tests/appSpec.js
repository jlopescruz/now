/* Controller + Service */
describe('App test', function() {

	var scope,
		formAppCtrl,
		formService,
		httpMock;

	beforeEach(module('formApp'));

	beforeEach(inject(function($rootScope, $controller, $q) {

		scope = $rootScope.$new();
		formService = { saveForm : function () { return $q.defer().promise; } };
		formAppCtrl = $controller('formAppCtrl', {
			$scope : scope,
			formService : formService
		});
		
	}));

	beforeEach(inject(function($httpBackend){

		httpMocks = $httpBackend;

	}));


	it('assert karma is running fine', function() {
		expect(true).toBe(true);	
	});

	it('submitForm should not call saveForm() on formService if form is not valid', function() {

		spyOn(formService, 'saveForm');
		scope.submitForm(false);
		expect(formService.saveForm).not.toHaveBeenCalled();

	});

	it('submitForm should call saveForm() on formService if form is valid', function() {

		spyOn(formService, 'saveForm').andCallThrough();
		scope.user = { name : 'Joao Cruz' };
		scope.submitForm(true);
		expect(formService.saveForm).toHaveBeenCalledWith(scope.user);

	});

    it('should get login success', function() {
    	
    	httpMock.flush();
    	httpMocks.expect('POST', 'endpoint/form/data', {}).respond(200, "[{ success : 'true'}]");

	    formService.saveForm({name: 'Joao'})
	      .then(function(data) {
	        expect(data.success).toBeTruthy();
	    });
	 
	  	httpMocks.flush();

	});
});