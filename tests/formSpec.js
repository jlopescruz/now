/* Main app testing */
xdescribe ('Main angular app testing', function () {

	/* Making main app available */
	beforeEach(module('formApp'));

	it('Should have an angularjs defined app', function() {
    	expect(formApp).toBeDefined();
  	});

});

/* Controller testing */
describe ('formController testing', function () {
	var $scope,
	    formService;

	/* Making main app available */
	beforeEach(module('formApp'));

	/* Making controller $scope available */
	beforeEach(inject(function($rootScope, $controller) {
        $scope = $rootScope.$new();
        //TODO: verificar como passar mais do que uma dependencia (e.g.: servico)
        $controller('formAppCtrl', {$scope: $scope});
    }));

	xit('Should have a valid form error message string', function() {
		expect(typeof $scope.inputErrorMsg).toBe('string');
	});

	xit('Should return false is the form is not valid', function () {
		expect($scope.submitForm(false)).toBe(false);
	});

	it('Should use service to store data', function () {
		
	});

});

/* Service testing */
describe ('formService testing', function () {

	xit('Should reject promise if parameter is not a valid object', inject(function(formService) {
		var aux = false;

		formService.saveForm('test').then(function(){},function(){
			aux=true;
			expect(aux).toBe(true);
		});
	}));

    xit('should get login success', inject(function(formService, $httpBackend) {
	    $httpBackend.expect('POST', 'endpoint/form/data')
	      .respond(200, "[{ success : 'true'}]");
	 
	    formService.saveForm({name: 'Joao'})
	      .then(function(data) {
	        expect(data.success).toBeTruthy();
	    });
	 
	  $httpBackend.flush();
	}));

});