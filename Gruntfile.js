module.exports = function (grunt) {

	grunt.initConfig({
	 	karma: {
            unit: {
                configFile: 'karma.conf.js',
                options: {
                    frameworks: ['jasmine'],
                    files: ['app/libs/angular.min.js',
                            'app/libs/angular-mocks-0.10.0.js',
                            'app/js/app.js',
                            'tests/appSpec.js'],
                    browsers: ['PhantomJS'],
                    reporters: ['progress', 'coverage'],
                    preprocessors: {
                      'app/js/app.js': ['coverage']
                    },
                    coverageReporter: {
                        reporters:[
                          {type: 'html', dir:'tests/coverage/'}
                        ]
                    },
                    singleRun: false
                }
                
            }
        }
	});

	grunt.loadNpmTasks('grunt-karma');

	grunt.registerTask('tests', ['karma:unit']);
};